# datapacks

## `origins_diet/`
add [origins](https://modrinth.com/mod/origins) "carnivores" compatibility to [farmer's delight](https://modrinth.com/mod/farmers-delight) and [expanded delight](https://modrinth.com/mod/expanded-delight)

## `origins_more/`
add a custom [origins](https://modrinth.com/mod/origins), a big icarae which is similar to the icarae added by the [icarus](https://modrinth.com/mod/icarus) mod but 3 blocks tall  
but it have the ability to shift between being 3 and 2 blocks tall

## `mystical_agriculture_fixes/`
fix Mystical Agriculture and it's integration with [Applied Energistics 2](https://modrinth.com/mod/ae2) and [EMI](https://modrinth.com/mod/emi) by adding seeds recipes in the crafting table and with essence upgrades not requiering Infusion Crystals, the latter because AE2 was eating them when crafting