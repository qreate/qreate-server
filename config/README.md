# configs
details of recommended server configs

## `config/byg/nether-biomes.json5`
personal preference, "oh the biomes you'll go" nether biomes are a bit too dense for my liking, but i still want them !  
luckily, you can simply increase the weights of vanilla biomes generating, making BYG's biomes rarer  
an example would look something like that, with vanilla biomes being 3 times more likely to generate compared to BYG's biomes
```json5
"biomeWeights": [
	{
		"data": "minecraft:crimson_forest",
		"weight": 6
	},
	{
		"data": "byg:weeping_mire",
		"weight": 2
	},
```

## `config/charm.toml`
disabling charm's totem of preserving, we already have the gravestone mod and it works better in most instances
```toml
L176
#The player's inventory items will be preserved in the Totem of Preserving upon death.
#By default, a new totem will always be spawned to preserve items upon dying ('Grave mode').
#If Grave mode is not enabled, you must be holding a totem in order for it to preserve your items.
"TotemOfPreserving Enabled" = false
```
```toml
L394
[TotemOfPreserving]
	#If true, a new totem will be spawned to preserve items upon dying.
	#Players do NOT need to be holding a Totem of Preserving.
	"Grave mode" = false
```

## `config/fwaystones/config.json5`
the following config gets rid of the xp cost to use waystones
```json5
L8
	"teleportation_cost": {
		"cost_type": "LEVEL",
		"cost_item": "minecraft:ender_pearl",
		"base_cost": 0,
		"cost_per_block_distance": 0.0,
		"cost_multiplier_between_dimensions": 0.0
	},
```

## `config/voicechat/voicechat-server.properties`
[exarotron](https://exaroton.com) specific, since exarotron only give you a single port per server, you need to use the same as the server port by leaving the `bind_address` empty
```properties
# The port of the voice chat server
# Setting this to "-1" sets the port to the Minecraft servers port (Not recommended)
port=-1
```

<!--
- ## `template`
desc
```json5
config
```
-->